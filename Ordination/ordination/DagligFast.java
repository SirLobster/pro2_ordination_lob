package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {
	private final Dosis[] dosis = new Dosis[4];
	private double morgenAntal;
	private double middagAntal;
	private double aftenAntal;
	private double natAntal;

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double morgenAntal,
			double middagAntal, double aftenAntal, double natAntal) {
		super(startDen, slutDen, laegemiddel);
		this.morgenAntal = morgenAntal;
		this.middagAntal = middagAntal;
		this.aftenAntal = aftenAntal;
		this.natAntal = natAntal;

	}

	public Dosis[] getDoser() {

		return dosis;
	}

	public void opretDosis() {
		Dosis d = new Dosis(LocalTime.of(8, 00), morgenAntal);
		dosis[0] = d;
		Dosis d2 = new Dosis(LocalTime.of(12, 00), middagAntal);
		dosis[1] = d2;
		Dosis d3 = new Dosis(LocalTime.of(18, 00), aftenAntal);
		dosis[2] = d3;
		Dosis d4 = new Dosis(LocalTime.of(22, 00), natAntal);
		dosis[3] = d4;

	}

	@Override
	public double samletDosis() {
//		int sum = 0;
//
//		for (Dosis d : dosis) {
//			sum += d.getAntal();
//		}
//
//		return sum * ChronoUnit.DAYS.between(getStartDen(), getSlutDen());
		return doegnDosis() * antalDage();
	}

	@Override
	public double doegnDosis() {
		double sum = 0;

		for (Dosis d : dosis) {
			sum += d.getAntal();
		}

		return sum;
	}

	@Override
	public String getType() {
		return "DagligFast";
	}

}
