package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {
	private double antalEnheder;
	private LocalDate startDen;
	private LocalDate slutDen;
	private Laegemiddel laegemiddel;
	private ArrayList<LocalDate> givetDatoer = new ArrayList<>();
	private int antalGangeGivet = 0;

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antal) {
		super(startDen, slutDen, laegemiddel);
		antalEnheder = antal;
		this.startDen = startDen;
		this.slutDen = slutDen;
		this.laegemiddel = laegemiddel;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if (givesDen.isAfter(startDen) && givesDen.isBefore(slutDen) || givesDen.equals(startDen)
				|| givesDen.equals(slutDen)) {
			antalGangeGivet++;
			givetDatoer.add(givesDen);
			return true;
		}

		return false;
	}

	public double doegnDosis() {
		LocalDate templdMin = LocalDate.MAX;
		LocalDate templdMax = LocalDate.MIN;
		for (LocalDate ld : givetDatoer) {
			if (ld.isAfter(templdMax))
				templdMax = ld;
			if (ld.isBefore(templdMin))
				templdMin = ld;
		}
		int daysBetween = (int) ChronoUnit.DAYS.between(templdMin, templdMax) + 1;

		return samletDosis() / daysBetween;
	}

	public double samletDosis() {
		return antalGangeGivet * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {

		return antalGangeGivet;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	public Laegemiddel getLaegemiddel() {

		return laegemiddel;
	}

	@Override
	public String getType() {

		return "PN";
	}

}
