package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> skævDoser = new ArrayList<Dosis>();
	private double[] antalEnheder;
	private LocalTime[] klokkeSlet;

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, LocalTime[] klokkeSlet,
			double[] antalEnheder) {
		super(startDen, slutDen, laegemiddel);
		this.klokkeSlet = klokkeSlet;
		this.antalEnheder = antalEnheder;

	}

	public void opretDosis() {
		int i = 0;
		while (i < klokkeSlet.length) {
			Dosis dosis = new Dosis(klokkeSlet[i], antalEnheder[i]);
			skævDoser.add(dosis);
			i++;
		}
	}

	public LocalTime[] getKlokkeSlet() {
		return klokkeSlet;
	}

	public LocalTime getKlokkeSlet(int i) {
		return klokkeSlet[i];
	}

	public double[] getAntalEnheder() {
		return antalEnheder;
	}

	public double getAntalEnheder(int i) {
		return antalEnheder[i];
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(skævDoser);

	}

	@Override
	public double samletDosis() {
		double totalDosis = 0;
		for (int i = 0; i < antalEnheder.length; i++) {
			totalDosis += skævDoser.get(i).getAntal();
		}
		return totalDosis * antalDage();
	}

	@Override
	public double doegnDosis() {
		return samletDosis() / antalDage();
	}

	@Override
	public String getType() {
		return "Daglig skæv";
	}
}
