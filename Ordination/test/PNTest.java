package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;

public class PNTest {

	public double samletDosisSetup(double givet, double antal) {
		LocalDate SLD = LocalDate.of(2020, 03, 01);
		LocalDate STD = LocalDate.of(2020, 03, 10);
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		PN pn = new PN(SLD, STD, l, antal);
		for (int i = 0; i < givet; i++)
			pn.givDosis(LocalDate.of(2020, 03, 05));
		return pn.samletDosis();
	}

	public double doegnDosisSetup(LocalDate dosis1, LocalDate dosis2) {
		LocalDate SLD = LocalDate.of(2020, 03, 01);
		LocalDate STD = LocalDate.of(2020, 03, 10);
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		PN pn = new PN(SLD, STD, l, 1);
		pn.givDosis(dosis1);
		pn.givDosis(dosis2);
		return pn.doegnDosis();
	}

	public boolean givDosisSetup(LocalDate givesDen) {
		LocalDate SLD = LocalDate.of(2020, 03, 01);
		LocalDate STD = LocalDate.of(2020, 03, 10);
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		PN pn = new PN(SLD, STD, l, 1);
		return pn.givDosis(givesDen);
	}

	@Test
	public void testGivDosis() {
//		fail("Not yet implemented");
		assertFalse(givDosisSetup(LocalDate.of(2020, 02, 25)));
		assertTrue(givDosisSetup(LocalDate.of(2020, 03, 01)));
		assertTrue(givDosisSetup(LocalDate.of(2020, 03, 05)));
		assertTrue(givDosisSetup(LocalDate.of(2020, 03, 10)));
		assertFalse(givDosisSetup(LocalDate.of(2020, 03, 11)));
	}

	@Test
	public void testSamletDosis() {
		assertEquals(30, samletDosisSetup(15, 2), 0.00001);
		assertEquals(0, samletDosisSetup(0, 0), 0.00001);
		assertEquals(10000, samletDosisSetup(100, 100), 0.00001);
		assertEquals(0, samletDosisSetup(0, 100), 0.00001);
		assertEquals(0, samletDosisSetup(100, 0), 0.00001);
	}

	@Test
	public void testDoegnDosis() {
		assertEquals(1, doegnDosisSetup(LocalDate.of(2020, 03, 04), LocalDate.of(2020, 03, 05)), 0.0001);
		assertEquals(0.2857, doegnDosisSetup(LocalDate.of(2020, 03, 04), LocalDate.of(2020, 03, 10)), 0.0001);
		assertEquals(0.4, doegnDosisSetup(LocalDate.of(2020, 03, 01), LocalDate.of(2020, 03, 05)), 0.0001);
		assertEquals(0.2, doegnDosisSetup(LocalDate.of(2020, 03, 01), LocalDate.of(2020, 03, 10)), 0.0001);

//		fail("Not yet implemented");
	}

}
