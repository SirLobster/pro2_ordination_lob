package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {

	@Test
	public void testOpretPNOrdination() {
		LocalDate startDen = LocalDate.of(2020, 03, 01);
		LocalDate slutDen = LocalDate.of(2020, 03, 10);
		Patient p = new Patient("777777-7777", "Bertel Haarder", 121);
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		PN pn = new PN(startDen, slutDen, l, 1);
		PN pnTester = Controller.getTestController().opretPNOrdination(startDen, slutDen, p, l, 1);
		assertEquals(pn.getAntalEnheder(), pnTester.getAntalEnheder(), 0.00001);
		assertEquals(pn.getLaegemiddel(), pnTester.getLaegemiddel());
		assertEquals(pn.getType(), pnTester.getType());
	}

	@Test
	public void testOpretDagligFastOrdination() {
		LocalDate startDen = LocalDate.of(2020, 03, 01);
		LocalDate slutDen = LocalDate.of(2020, 03, 10);
		Patient p = new Patient("777777-7777", "Bertel Haarder", 121);
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		DagligFast df = new DagligFast(startDen, slutDen, l, 2, 4, 3, 1);
		DagligFast dfTester = Controller.getTestController().opretDagligFastOrdination(startDen, slutDen, p, l, 2, 4, 3,
				1);
		assertEquals(df.getSlutDen(), dfTester.getSlutDen());
		assertEquals(df.getLaegemiddel(), dfTester.getLaegemiddel());
		assertEquals(df.getType(), dfTester.getType());
	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		LocalDate startDen = LocalDate.of(2020, 03, 01);
		LocalDate slutDen = LocalDate.of(2020, 03, 10);
		Patient p = new Patient("777777-7777", "Bertel Haarder", 121);
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		LocalTime[] klokkeSlet = { LocalTime.of(12, 0), LocalTime.of(13, 00), LocalTime.of(17, 0) };
		double[] antalEnheder = { 3, 5, 0 };
		DagligSkaev ds = new DagligSkaev(startDen, slutDen, l, klokkeSlet, antalEnheder);
		DagligSkaev dsTester = Controller.getTestController().opretDagligSkaevOrdination(startDen, slutDen, p, l,
				klokkeSlet, antalEnheder);
		assertEquals(ds.getAntalEnheder(), dsTester.getAntalEnheder());
		assertEquals(ds.getLaegemiddel(), dsTester.getLaegemiddel());
		assertEquals(ds.getType(), dsTester.getType());
	}

	@Test
	public void testOrdinationPNAnvendt() {

		LocalDate SLD = LocalDate.of(2020, 03, 01);
		LocalDate STD = LocalDate.of(2020, 03, 10);
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		PN pn = new PN(SLD, STD, l, 1);
		Controller.getTestController().ordinationPNAnvendt(pn, LocalDate.of(2020, 03, 1));
		assertEquals(1, pn.getAntalGangeGivet(), 0.01);
		Controller.getTestController().ordinationPNAnvendt(pn, LocalDate.of(2020, 03, 05));
		assertEquals(2, pn.getAntalGangeGivet(), 0.01);
		Controller.getTestController().ordinationPNAnvendt(pn, LocalDate.of(2020, 03, 10));
		assertEquals(3, pn.getAntalGangeGivet(), 0.01);
	}

	@Test
	public void testAnbefaletDosisPrDoegn() {
		Patient p1 = new Patient("111111-1111", "BD Gorilla", 0);
		Patient p2 = new Patient("222222-2222", "L, Birdsong", 7);
		Patient p3 = new Patient("555555-5555", "A, Håndled", 57);
		Patient p4 = new Patient("777777-7777", "Bertel Haarder", 121);
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		assertEquals(0, Controller.getTestController().anbefaletDosisPrDoegn(p1, l), 0.00001);
		assertEquals(0.7, Controller.getTestController().anbefaletDosisPrDoegn(p2, l), 0.00001);
		assertEquals(8.55, Controller.getTestController().anbefaletDosisPrDoegn(p3, l), 0.00001);
		assertEquals(19.36, Controller.getTestController().anbefaletDosisPrDoegn(p4, l), 0.00001);

	}
}
