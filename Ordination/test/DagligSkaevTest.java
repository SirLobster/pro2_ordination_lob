package test;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Assert;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligSkaevTest {

	private LocalTime[] klokkeSlet1 = { LocalTime.of(12, 0), LocalTime.of(13, 00), LocalTime.of(17, 0) };
	private double[] antalEnheder1 = { 3, 5, 0 };

	private LocalTime[] klokkeSlet2 = { LocalTime.of(12, 0), LocalTime.of(14, 00), LocalTime.of(16, 0) };
	private double[] antalEnheder2 = { 4, 8, 3 };

	private LocalTime[] klokkeSlet3 = { LocalTime.of(12, 0), LocalTime.of(14, 00), LocalTime.of(16, 0) };
	private double[] antalEnheder3 = { 12, 0, 6 };

	private LocalTime[] klokkeSlet4 = { LocalTime.of(12, 0), LocalTime.of(14, 00), LocalTime.of(16, 0) };
	private double[] antalEnheder4 = { 0, 0, 0 };

	private Patient p1 = new Patient("121256-0512", "Jane Jensen", 63.4);
	private Laegemiddel l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");

	private DagligSkaev ds1 = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24), l1, klokkeSlet1,
			antalEnheder1);

	// 7 dage i mellem
	private DagligSkaev ds2 = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 29), l1, klokkeSlet2,
			antalEnheder2);
	// 7 dage i mellem
	private DagligSkaev ds3 = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 29), l1, klokkeSlet3,
			antalEnheder3);
	// 7 dage i mellem
	private DagligSkaev ds4 = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 29), l1, klokkeSlet4,
			antalEnheder4);

	public void setUp() throws Exception {
		p1.addOrdinering(ds1);
		p1.addOrdinering(ds2);
		p1.addOrdinering(ds3);
		p1.addOrdinering(ds4);
		ds1.opretDosis();
		ds2.opretDosis();
		ds3.opretDosis();
		ds4.opretDosis();
	}

	@Test
	public void testOpretDosis1() {
		Assert.assertArrayEquals(antalEnheder1, ds1.getAntalEnheder(), 0);
		Assert.assertArrayEquals(klokkeSlet1, ds1.getKlokkeSlet());
	}

	@Test
	public void testOpretDosis2() {
		Assert.assertArrayEquals(antalEnheder2, ds2.getAntalEnheder(), 0);
		Assert.assertArrayEquals(klokkeSlet2, ds2.getKlokkeSlet());
	}

	@Test
	public void testOpretDosis3() {
		Assert.assertArrayEquals(antalEnheder3, ds3.getAntalEnheder(), 0);
		Assert.assertArrayEquals(klokkeSlet3, ds3.getKlokkeSlet());
	}

	@Test
	public void testOpretDosis4() {
		Assert.assertArrayEquals(antalEnheder4, ds4.getAntalEnheder(), 0);
		Assert.assertArrayEquals(klokkeSlet4, ds4.getKlokkeSlet());
	}

	// -----------------------------------------------------------------------

	@Test
	public void testSamletDosis1() {
		p1.addOrdinering(ds1);
		p1.addOrdinering(ds2);
		p1.addOrdinering(ds3);
		p1.addOrdinering(ds4);
		ds1.opretDosis();
		ds2.opretDosis();
		ds3.opretDosis();
		ds4.opretDosis();

		double sum1 = p1.getOrdinationer().get(1).samletDosis();
		Assert.assertEquals(105, sum1, 1);
	}

	@Test
	public void testSamletDosis2() {
		p1.addOrdinering(ds1);
		p1.addOrdinering(ds2);
		p1.addOrdinering(ds3);
		p1.addOrdinering(ds4);
		ds1.opretDosis();
		ds2.opretDosis();
		ds3.opretDosis();
		ds4.opretDosis();
		double sum2 = p1.getOrdinationer().get(2).samletDosis();
		Assert.assertEquals(126, sum2, 1);
	}

	@Test
	public void testSamletDosis3() {
		p1.addOrdinering(ds1);
		p1.addOrdinering(ds2);
		p1.addOrdinering(ds3);
		p1.addOrdinering(ds4);
		ds1.opretDosis();
		ds2.opretDosis();
		ds3.opretDosis();
		ds4.opretDosis();
		double sum3 = p1.getOrdinationer().get(3).samletDosis();
		Assert.assertEquals(0, sum3, 1);
	}

	// ------------------------------------------------------------------------

	@Test
	public void testDoegnDosis1() {

		p1.addOrdinering(ds1);
		p1.addOrdinering(ds2);
		p1.addOrdinering(ds3);
		p1.addOrdinering(ds4);
		ds1.opretDosis();
		ds2.opretDosis();
		ds3.opretDosis();
		ds4.opretDosis();
		double sum1 = p1.getOrdinationer().get(1).doegnDosis();
		Assert.assertEquals(15, sum1, 1);
	}

	@Test
	public void testDoegnDosis2() {

		p1.addOrdinering(ds1);
		p1.addOrdinering(ds2);
		p1.addOrdinering(ds3);
		p1.addOrdinering(ds4);
		ds1.opretDosis();
		ds2.opretDosis();
		ds3.opretDosis();
		ds4.opretDosis();
		double sum2 = p1.getOrdinationer().get(2).doegnDosis();
		Assert.assertEquals(18, sum2, 1);
	}

	@Test
	public void testDoegnDosis3() {

		p1.addOrdinering(ds1);
		p1.addOrdinering(ds2);
		p1.addOrdinering(ds3);
		p1.addOrdinering(ds4);
		ds1.opretDosis();
		ds2.opretDosis();
		ds3.opretDosis();
		ds4.opretDosis();
		double sum3 = p1.getOrdinationer().get(3).doegnDosis();
		Assert.assertEquals(0, sum3, 1);
	}

}
