package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import ordination.DagligFast;
import ordination.Dosis;
import ordination.Laegemiddel;

public class DagligFastTest {
	
	Dosis d = new Dosis(LocalTime.of(8, 00), 4.0);
	Dosis d2 = new Dosis(LocalTime.of(12, 00), 1.0);
	Dosis d3 = new Dosis(LocalTime.of(18, 00), 2.0);
	Dosis d4 = new Dosis(LocalTime.of(22, 00), 1.0);
	Dosis d5 = new Dosis(LocalTime.of(8, 00), 0.0);
	Dosis d6 = new Dosis(LocalTime.of(12, 00), 1.0);
	Dosis d7 = new Dosis(LocalTime.of(18, 00), 0.0);
	Dosis d8 = new Dosis(LocalTime.of(22, 00), 1.0);
	
	Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	
	DagligFast df = new DagligFast(LocalDate.of(2020, 3, 1), LocalDate.of(2020, 3, 10), l, 4, 1, 2, 1);
	DagligFast df2 = new DagligFast(LocalDate.of(2020, 3, 1), LocalDate.of(2020, 3, 10), l, 0, 1, 0, 1);


	@Test
	public void opretDosistest() {
		Dosis[] dosis = new Dosis[4];
		dosis[0] = d;
		dosis[1] = d2;
		dosis[2] = d3;
		dosis[3] = d4;
		Dosis[] dosis2 = new Dosis[4];
		dosis2[0] = d5;
		dosis2[1] = d6;
		dosis2[2] = d7;
		dosis2[3] = d8;
		
		df.opretDosis();
		df2.opretDosis();
	
		for(int i = 0; i < 4; i++) {
			assertEquals(dosis[0].getAntal(), df.getDoser()[0].getAntal(), 0.001);
			assertEquals(dosis[0].getTid(), df.getDoser()[0].getTid());
			assertEquals(dosis2[0].getAntal(), df2.getDoser()[0].getAntal(), 0.001);
			assertEquals(dosis2[0].getTid(), df2.getDoser()[0].getTid());
		}
	}
	
	@Test
	public void samletDosistest() {
		df.opretDosis();
		df2.opretDosis();
		assertEquals(80, df.samletDosis(), 0.001);
		assertEquals(20, df2.samletDosis(), 0.001);
	}
	
	@Test
	public void doegnDosistest() {
		df.opretDosis();
		df2.opretDosis();
		assertEquals(8, df.doegnDosis(), 0.001);
		assertEquals(2, df2.doegnDosis(), 0.001);
	}

}
